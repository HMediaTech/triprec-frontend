var app = angular.module("TripRec", ['ngRoute', 'angularCSS', 
                                     'jkAngularRatingStars', 'angularjs-datetime-picker',
                                     'ngTagsInput', 'rzModule']);

app.config(['$routeProvider', function($routeProvider){

  $routeProvider
  .when('/', {
    templateUrl:'login.html',
    css: ['../../src/css/registration/styles.css'],
    controller:'LoginCtrl'
  })
  .when('/register',{
    templateUrl:'signup.html',
    css: ['../../src/css/registration/styles.css'],
    controller:'SignupCtrl'
  })
  .when('/hotels',{
    templateUrl: 'hotel_restaurant.html',
    css: ['../../src/css/hotel_restaurant/hotel_restaurant.css'],
    controller: 'HotelsCtrl'
  })
  .when('/packages',{
    templateUrl: 'budget_packages.html',
    css: ['../../src/css/budget_packages/budget_packages.css'],
    controller: 'PackagesCtrl'
  })
  .otherwise({
    redirectTo: '/'
  });

}]);

// doesn't allows to in without login, if user logged already
app.run(function($rootScope, $location, User) {
  $rootScope.$on("$locationChangeStart", function(event, next, current){
    // console.log(event, next, current);
    // checks if user is logged or not
    if($location.path().indexOf('register') > -1){
      $location.path('/register');
    }
    else if(!User.isLogged()){
      // if not redirect to login page
       $location.path('/');
     }
  });
});

// app service to hold users details and store in local storage
app.service('User', function (){
   // set user login variable to false
   this.login=false;
   // function to save access_token
   this.setToken = function(value){
    localStorage.setItem('token', value);
    this.login=true;
   }
   // function to get user's access_token
   this.getToken = function(){
    return localStorage.getItem('token');
   }
   // function to save user name
   this.setName = function(value){
    localStorage.setItem('name', value)
   }
   // function to get the user name
   this.getName = function(){
     return localStorage.getItem('name');
   }
   // function to save userID
   this.setUserID = function(value){
     localStorage.setItem('userID', value);
   }
   // function to get userID
   this.getUserID = function(){
     return localStorage.getItem('userID');
   }
   //logout function deletes the access_token from local storage
   this.logout = function(){
    localStorage.removeItem('token');
    localStorage.removeItem('name');
    localStorage.removeItem('userID');
    this.login=false;
   }
   // check if user logged in, return true or false
   this.isLogged = function(){
      if(localStorage.getItem('token')){
        this.login=true;
      }
    return this.login;
   }
});

// logout
app.controller('HeaderCtrl', function($scope, $location, $rootScope, User){
  $scope.Logout = function(){
    User.logout();
    $location.path('/');
  }
})
// load landing page
app.controller('LoginCtrl', function($scope, $location, $rootScope, $http, User){

   //user input to login variable
    $scope.login = {
      email: '',
      password: ''
    }

   // temp variablea
   $scope.user=false;
   $scope.registered=false;
   $scope.logged=true;

   // if user is logged move to /hotels page
   if(User.isLogged()){
    $location.path('/hotels');
   }

   $scope.submit=function(e){
       e.preventDefault();
       $scope.logged=false;

       var login_data = JSON.stringify($scope.login);
       var login_url='http://127.0.0.1:5000/login';

       $http({method: 'POST', url: login_url, data: login_data})
        .then(function (response){
          console.log(response);
        // save access_token, and name
          User.setToken(response.data.token);
          User.setName(response.data.username);
          User.setUserID(response.data.userID);
          $location.path('/hotels');
        },function (error){
          $location.path('/');
          $scope.user=true;
          $scope.logged=true;
          $scope.error_message = error.data;
       });
    }
 });


app.controller('SignupCtrl', function($scope, $location, $rootScope, $http){

   //user input to login variable
    $scope.register = {
      fname: '',
      lname: '',
      email: '',
      password: ''
    }

   // temp variablea
   $scope.registered=true;
   $scope.user=false;

   $scope.submit=function(e){
       e.preventDefault();
       $scope.registered=false;

       var register_data = JSON.stringify($scope.register);
       var register_url='http://127.0.0.1:5000/register';

       $http({method: 'POST', url: register_url, data: register_data})
        .then(function (response){
        // move to login page
          $location.path('/');
        },function (error){
          // show error message
          $location.path('/register');
          $scope.user=true;
          $scope.error_message = error.data;
          $scope.registered=true;
       });
    }
 });


// hotels page controller
app.controller('HotelsCtrl' , function($scope, $location, $http, prices, User){

  $scope.restaurants = false;
  $scope.hotel = true;
  $scope.waiting = true;
  var userID = User.getUserID();
  var req_data = {userID: userID}
  var data = null;
  var package_details = {};

  $http({method: 'POST', url: 'http://127.0.0.1:5000/hotels', data: JSON.stringify(req_data)}).then(success)
        // .then(function (response){
        //   $scope.hotels = response.data;
        // });
   
  function success(response){

     $scope.hotels = response.data;
     data = response.data;

     package_details = {
      ratings_results:{
        userID:userID,
        ratings: [{
           name: data[0].name,
           rate: 0,
           id: data[0].hotelID
          },
          {
           name: data[1].name,
           rate: 0,
           id: data[1].hotelID
          },
          {
           name: data[2].name,
           rate: 0,
           id: data[2].hotelID
          },
          {
           name: data[3].name,
           rate: 0,
           id: data[3].hotelID  
          },
          {
           name: data[4].name,
           rate: 0,
           id: data[4].hotelID
          },
          {
           name: data[5].name,
           rate: 0,
           id: data[5].hotelID
        }]
      }
     };
  }
   
   

   // load cuisines fro JSON file
   $scope.loadTags = function(query) {
     return $http.get('../../src/js/cuisines.json', { cache: true}).then(function(response) {
      var cuisines = response.data;
      return cuisines.filter(function(cuisine) {
        return cuisine.text.toLowerCase().indexOf(query.toLowerCase()) != -1;
      });
    });
   };

   // when rating is chosen
   $scope.onItemRating = function(rating, name){
    // find hotel name, update rating_results
    for(i in package_details.ratings_results.ratings){
      if(package_details.ratings_results.ratings[i].name == name){
         package_details.ratings_results.ratings[i].rate = rating;
      }
    }
   }
   // testing the result
   $scope.hotelRatings = function(){
      
      $scope.error = false;
      var empty = false;
      // check if there is any 0 rating
      for(var i=0; i<package_details.ratings_results.ratings.length; i++){
        if (package_details.ratings_results.ratings[i].rate == 0){
             empty = true;
             break;
        }
      }
      
      if(empty == true){
        $scope.error = true;
      }else{
        // hide hotels, show restaurants
        $scope.restaurants = true;
        $scope.hotel = false;
      }

    }

    $scope.packages = function(){
      var check_in = Date.parse($scope.check_in);
      var check_out = Date.parse($scope.check_out);

      var days = Math.floor((check_out-check_in)/(1000*60*60*24));
      var radius = 0;
      var times = 0;
      var cuisine = "";
      $scope.waiting = false;

      $scope.cuisine_error = false;

      var distance = $scope.radius
      if(distance === "Near hotel (0-500m)"){
         radius = 500
      } else if(distance === "Not far from hotel (0-1000m)"){
         radius = 1000
      } else{
         radius = 0
      }
      
      var rest_for = $scope.rest_for
      if(rest_for === "Both"){
        times = 2
      }else{
        times = 1
      }
      // get cuisines into string 
      for(var x in $scope.tags){
        if(x!=$scope.tags.length-1){
          cuisine+=$scope.tags[x].text+"%2C%20"
        }else{
          cuisine+=$scope.tags[x].text
        }
      }

      // replace ' ' with %20
      var cuisines  = cuisine.split(' ').join('%20');
      
      if(days > 0 && times != null){
        $scope.cuisine_error = false;
        // specify data in request data 
        package_details.cuisines = cuisines;
        package_details.days = days;
        package_details.radius = radius;
        package_details.times = times;

        var packaging_data = JSON.stringify(package_details);
        var packaging_url='http://127.0.0.1:5000/budget';
        
        $http({method: 'POST', url: packaging_url, data: packaging_data})
         .then(function (response){
          // set package prices
           prices.setJson(response.data);
           // move to packages page
           $location.path('/packages');
         },function (error){
           console.log(error);
        });
      }else{
        $scope.cuisine_error = true;
        $scope.waiting = true;
      }
    }

});

app.controller('PackagesCtrl' , function($scope, $http, prices){
   
    var prices  = prices.getJson();
    var url='http://127.0.0.1:5000/packages';
    
    $scope.budget_select = true;
    $scope.packages_page = false; 

    $scope.initialListener = function(sliderId) {

      var budget = JSON.stringify({budget:$scope.slider.value});

      // send budget to packages API
      $http({method: 'POST', url: url, data: budget})
       .then(function (response){
         $scope.verticalSlider.options.value = $scope.slider.value;
         $scope.verticalSlider.options.floor = prices.Min;
         $scope.verticalSlider.options.ceil = prices.Max;
         $scope.packages = response.data; 
         $scope.budget_select = false;
         $scope.packages_page = true;
       },function (error){
         console.log(error);
      });
    };

    $scope.slider = {
    value: prices.Avg,
    options: {
        showSelectionBar: true,
        floor: prices.Min,
        ceil: prices.Max,
        id: 'budgetSlider',
        onChange: $scope.initialListener,
        getSelectionBarColor: function(value) {
            if (value <= prices.First)
                return 'red';
            if (value <= prices.Avg)
                return 'orange';
            if (value <= prices.Third)
                return 'yellow';
            return '#2AE02A';
        },
        translate: function(value) {
           
            return '$' + value;
        }
      }
    };
    

    $scope.secondaryListener = function(sliderId) {

      var budget = JSON.stringify({budget:$scope.verticalSlider.value});
  
      // send budget to packages API
      $http({method: 'POST', url: url, data: budget})
       .then(function (response){
         $scope.packages = response.data;
       },function (error){
         console.log(error);
      });
    };
    

    // vertical slider
    $scope.verticalSlider = {
      value: $scope.slider.value,
      options: {
        floor: prices.Min,
        ceil: prices.Max,
        vertical: true,
        id: 'verticalBudgetSlider',
        onChange: $scope.secondaryListener,
      }
    };
    

});

app.factory('prices', function(){
    var package_prices = null;//the object to hold our data
     return {
     getJson:function(){
       return package_prices;
     },
     setJson:function(value){
      package_prices = value;
     }
   }
 });
