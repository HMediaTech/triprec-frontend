
//location details
var long = '';
var latt = '';

function initMap() {
        var uluru = {lat: 54.5260, lng: 15.2551};
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 2,
          center: uluru
        });

       //set limit to minimize the map zoom
       map.setOptions({ minZoom: 2 });

       // var card = document.getElementById('pac-card');
       var input = document.getElementById('location');

       var autocomplete = new google.maps.places.Autocomplete(input);

       // Bind the map's bounds (viewport) property to the autocomplete object,
       // so that the autocomplete requests use the current map bounds for the
       // bounds option in the request.
       autocomplete.bindTo('bounds', map);

       var infowindow = new google.maps.InfoWindow();
       var infowindowContent = document.getElementById('infowindow-content');
       infowindow.setContent(infowindowContent);
       var marker = new google.maps.Marker({
         map: map,
         anchorPoint: new google.maps.Point(0, -29)
       });

       autocomplete.addListener('place_changed', function() {

         infowindow.close();
         marker.setVisible(false);
         var place = autocomplete.getPlace();

         // if place is changed get the long and latitude
         long = place.geometry.location.lng();
         latt = place.geometry.location.lat();

         if (!place.geometry) {
           // User entered the name of a Place that was not suggested and
           // pressed the Enter key, or the Place Details request failed.
           window.alert("No details available for input: '" + place.name + "'");
           return;
         }

         // If the place has a geometry, then present it on a map.
         if (place.geometry.viewport) {
           map.fitBounds(place.geometry.viewport);
         } else {
           map.setCenter(place.geometry.location);
           map.setZoom(17);  // Why 17? Because it looks good.
         }
         marker.setPosition(place.geometry.location);
         marker.setVisible(true);

         var address = '';
         if (place.address_components) {
           address = [
             (place.address_components[0] && place.address_components[0].short_name || ''),
             (place.address_components[1] && place.address_components[1].short_name || ''),
             (place.address_components[2] && place.address_components[2].short_name || '')
           ].join(' ');
         }

         infowindowContent.children['place-icon'].src = place.icon;
         infowindowContent.children['place-name'].textContent = place.name;
         infowindowContent.children['place-address'].textContent = address;
         infowindow.open(map, marker);
       });
}

//make asynchronous call to the server, get the restaurants details
function Search(){

  console.log("latitude of the selected location: "+latt);
  console.log("longtitude of the selected location: "+long);

  //make asynchronous call to the zomato api
  // $.ajax({
  //   type: 'GET',
  //   url: 'https://developers.zomato.com/api/v2.1/geocode?lat='+lat+'&lon='+long,
  //   headers: {'user-key':'8cfd1aa4fc1be27d56f3fb82265ee001'},
  //   success: function(rest){
  //     console.log(rest.nearby_restaurants);
  //
  //     //initialize the names of the restaurant
  //
  //
  //     for(var i=0; i<rest.nearby_restaurants.length; i++){
  //       console.log("restaurant around selected place: "+rest.nearby_restaurants[i].restaurant.name);
  //     }
  //
  //   }
  // });

}
